package com.zuitt;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class UserServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7560556849380195433L;
	
	public void init() throws ServletException {   
        System.out.println("******************************************");
        System.out.println("UserServlet has been initialized. ");
        System.out.println("******************************************");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException{
		
		// Store the first name as a system property
        String firstName = req.getParameter("firstName");
        System.getProperties().put("firstName",firstName);
        
        // Get the HttpSession object and store the last name as a session attribute
        String lastName = req.getParameter("lastName");
        HttpSession session = req.getSession();
        session.setAttribute("lastName", lastName);
        
        // Get the ServletContext object and store the email as a context attribute
        String email = req.getParameter("email");
        getServletContext().setAttribute("email", email);
        
        String contactNumber = req.getParameter("contactNumber");
        // Url Rewriting via sendRedirect Method
        res.sendRedirect("details?contactNumber="+contactNumber);
		
	}
	
	public void destroy() {

		
		System.out.println("***************************");
		System.out.println("UserServlet has been Destroy.");
		System.out.println("***************************");
		
	}


}
