package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class DetailsServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4484146589817098769L;
	
	public void init() throws ServletException {   
        System.out.println("******************************************");
        System.out.println("DetailsServlet has been initialized. ");
        System.out.println("******************************************");
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
		
		HttpSession session = req.getSession();
		ServletContext srvContext = getServletContext();

		//Retrieves Data
		String branding = srvContext.getInitParameter("branding");
		String firstName = System.getProperty("firstName");
		String lastName = session.getAttribute("lastName").toString();
		String email = srvContext.getAttribute("email").toString();
		String contactNumber = req.getParameter("contactNumber");
        
		PrintWriter out = res.getWriter();
        
		//Displays the retrieve output 
		out.println(
				
		"<h1>"+ branding +"</h1>" +
		"<p> First Name: " + firstName + "</p>"+
		"<p> Last Name: " + lastName + "</p>" +
		"<p> Contact: " + contactNumber + "</p>" +
		"<p> Email: " + email + "</p>"
				
	);

}

	public void destroy() {

		System.out.println("***************************");
		System.out.println("DetailsServlet has been Destroy.");
		System.out.println("***************************");
	}
	
}
